import React from 'react';
import {NavLink} from "react-router-dom";

let Navbar = () =>{
    return (
        <React.Fragment>
           <nav className="navbar navbar-dark bg-success navbar-expand-sm">
               <div className="container">
                   <NavLink to="/" className="navbar-brand">React Router</NavLink>
                   <div className="collapse navbar-collapse">
                       <ul className="navbar-nav me-auto">
                           <li className="nav-item">
                               <NavLink to={'/employees/list'} className="nav-link">Employees</NavLink>
                           </li>
                           <li className="nav-item">
                               <NavLink to={'/countries/list'} className="nav-link">Countries</NavLink>
                           </li>
                           <li className="nav-item">
                               <NavLink to={'/about'} className="nav-link">About</NavLink>
                           </li>
                       </ul>
                       <div className="d-flex">
                           <ul className="navbar-nav">
                               <li className="nav-item">
                                   <NavLink to={'/user/register'} className="nav-link">Register</NavLink>
                               </li>
                               <li className="nav-item">
                                   <NavLink to={'/user/login'} className="nav-link">Login</NavLink>
                               </li>

                           </ul>
                       </div>
                   </div>
               </div>
           </nav>
        </React.Fragment>
    )
}

export default Navbar