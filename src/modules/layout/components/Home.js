import React from 'react';
import {NavLink} from "react-router-dom";


let Home = () =>{
    return (
        <React.Fragment>
            <div className="landing-page">
                <div className="wrapper">
                    <div className='d-flex flex-column align-items-center justify-content-center text-center h-100'>
                        <h3 className='display-3'>React Router</h3>
                        <p>
                            Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ab accusantium adipisci asperiores
                            aspernatur dignissimos dolore eos et explicabo fuga, iure nam natus nisi perspiciatis rem,
                            rerum sapiente similique sit sunt?
                        </p>
                        <div>
                            <NavLink to={'/user/login'} className="btn btn-success btn-sm">Login</NavLink>
                            <NavLink to={'/user/register'} className="btn btn-light btn-sm">Register</NavLink>
                        </div>
                    </div>
                </div>
            </div>
        </React.Fragment>
    )
}

export default Home