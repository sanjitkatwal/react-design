import React, {useEffect, useState} from 'react';
import {NavLink, useParams} from "react-router-dom";
import {EmployeeService} from "../services";
import Spinner from "../../layout/components/Spinner";


let EmployeeDetail = () =>{
    let {employeeID} = useParams();

    let [state, setstate] = useState({
        loading:false,
        employee : '',
        errorMessage: ''
    })

    useEffect(() => {
        setstate({...state, loading:true})
    EmployeeService.getEmployee(employeeID).then(({employee}) => {
        setstate({
            ...state,
            loading:false,
            employee: employee
        })
    }).catch((error) => {
        setstate({
            ...state,
            loading:false,
            errorMessage: error.message
        })
    })
    }, [employeeID])

    let {loading, employee, errorMessage} = state

    return (
        <React.Fragment>
            <div className="container">
                <div className="row">
                    <h4>Details of employee</h4>
                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Alias consectetur dolor magnam nisi vel.
                        Dignissimos excepturi fuga nostrum perspiciatis similique. Consequatur dolor eum incidunt ipsa laudantium
                        provident quae rem repellendus!
                    </p>
                </div>
            </div>
            {
                loading ? <Spinner /> : <React.Fragment>
                    {
                        Object.keys(employee).length > 0 &&
                        <section>
                            <div className="container">
                                <div className="row">
                                    <div className="col-md-8">
                                        <div className="card">
                                            <div className="car-header bg-success text-white">
                                                <p className="h4">{employee.name.first} {employee.name.middle} {employee.name.last}</p>
                                            </div>

                                            <div className="card-body">
                                                <div className="row">
                                                    <div className="col-md-3">
                                                        <img src={employee.picture.large} alt="" className="img-fluid"/>
                                                    </div>
                                                    <div className="col-md-9">
                                                        <ul className="list-group">
                                                            <li className="list-group-item list-group-item-success">Gender: {employee.gender}</li>
                                                            <li className="list-group-item list-group-item-success">Age: {employee.dob.age}</li>
                                                            <li className="list-group-item list-group-item-success">Email: {employee.email}</li>
                                                            <li className="list-group-item list-group-item-success">Location: {employee.location.city}</li>
                                                        </ul>
                                                    </div>
                                                </div>
                                            </div>
                                            <div className="card-footer">
                                                <NavLink to={'/employees/list'} className="btn btn-success btn-sm">Back</NavLink>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </section>
                    }
                </React.Fragment>
            }
        </React.Fragment>
    )
}

export default EmployeeDetail