import React, {useEffect, useState} from 'react';
import {EmployeeService} from "../services";
import Spinner from "../../layout/components/Spinner";
import {NavLink} from "react-router-dom";


let EmployeeList = () =>{
    let [state, setState] = useState({
        loading:false,
        employees : [],
        errorMessage:''
    })

    useEffect(() => {
        setState({...state, loading : true})
        EmployeeService.getAllEmployees().then((response) => {
            setState({
                ...state,
                loading: false,
                employees : response.data

            })
        }).catch((error) => {
            setState({
                ...state,
                loading: false,
                errorMessage : error.message

            })
        })
    }, [])

    let {loading, employees, errorMessage} = state
    return (
        <React.Fragment>
            <section>
                <div className="container mt-3">
                    <div className="row">
                        <div className="col">
                            <p className="h4 text-success">Employee List</p>
                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Alias amet architecto eius libero
                                maiores modi numquam veritatis! A ab at consequuntur ducimus facilis fugit iusto quo quod,
                                voluptate voluptates voluptatibus!</p>
                        </div>
                    </div>
                </div>
            </section>
            {
                loading ? <Spinner /> : <React.Fragment>
                    {
                        employees.length > 0 &&
                            <section>
                                <div className="container">
                                    <div className="row">
                                        <div className="col">
                                            <table className="table table-hover text-center table-striped">
                                                <thead className="bg-success text-white">
                                                <tr>
                                                    <th>SNO</th>
                                                    <th>Image</th>
                                                    <th>Name</th>
                                                    <th>Age</th>
                                                    <th>Email</th>
                                                    <th>Location</th>
                                                </tr>
                                                </thead>
                                                <tbody>
                                                {
                                                    employees.map(employee => {
                                                        return(
                                                            <tr key={employee.login.uuid}>
                                                                <td>{employee.login.uuid.substr(employee.login.uuid.length - 5)}</td>
                                                                <td>
                                                                    <img src={employee.picture.large} alt="" width={50} height={50}/>
                                                                </td>
                                                                <td>
                                                                    <NavLink to={`/employee/${employee.login.uuid}`} className="text-success">
                                                                        {employee.name.first} {employee.name.last}
                                                                    </NavLink>
                                                                </td>
                                                                <td>{employee.dob.age} yrs</td>
                                                                <td>{employee.email}</td>
                                                                <td>{employee.location.city}</td>
                                                            </tr>
                                                        )
                                                    })
                                                }
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </section>
                    }
                </React.Fragment>
            }

        </React.Fragment>
    )
}

export default EmployeeList