import axios from "axios";

export class EmployeeService{

     static  getAllEmployees(){
         let serverUrl= `https://gist.githubusercontent.com/thenaveensaggam/11ef016cec86cb68f8c28b2683d6c6eb/raw/86bd9c5684abe1c7f5ae81b720a11ad974ed6417/employees.26.4.2021.json`
         return axios.get(serverUrl)

    }

    static getEmployee(employeeID){
        let serverUrl= `https://gist.githubusercontent.com/thenaveensaggam/11ef016cec86cb68f8c28b2683d6c6eb/raw/86bd9c5684abe1c7f5ae81b720a11ad974ed6417/employees.26.4.2021.json`
        return new Promise((resolve, reject)=>{
            axios.get(serverUrl).then((response) =>{
                let employees= response.data
                let employee = employees.find(employee => employee.login.uuid === employeeID)
                resolve({employee})
            }).catch((error) => {
                reject(error)
            })
        })
    }
}