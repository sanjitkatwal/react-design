import React from 'react';
import {NavLink} from "react-router-dom";


let RegisterUser = () =>{
    return (
        <React.Fragment>

            <section>
                <div className="container mt-3">
                    <div className="row">
                        <div className="col">
                            <p className="h4 text-success">Register Here</p>
                        </div>
                    </div>
                </div>
            </section>
            <section>
                <div className="container">
                    <div className="row">
                        <div className="col-md-4">
                            <form action="">
                                <div className="mb-2">
                                    <input type="text" className="form-control" placeholder="User Name"/>
                                </div>
                                <div className="mb-2">
                                    <input type="email" className="form-control" placeholder="Email"/>
                                </div>
                                <div className="mb-2">
                                    <input type="password" className="form-control" placeholder="Password"/>
                                </div>

                                <div className="mb-2">
                                    <input type="submit" className="btn btn-success btn-sm" value='Register'/>
                                </div>
                            </form>
                            <small>
                                Already have an Account ?
                                <NavLink to={'/user/login'} className="text-success"> Login</NavLink>
                            </small>
                        </div>
                    </div>
                </div>
            </section>
        </React.Fragment>
    )
}

export default RegisterUser