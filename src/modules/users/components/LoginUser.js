import React from 'react';
import {NavLink} from "react-router-dom";


let LoginUser = () =>{
    return (
        <React.Fragment>

            <section>
                <div className="container mt-3">
                    <div className="row">
                        <div className="col">
                            <p className="h4 text-success">Login Here</p>
                        </div>
                    </div>
                </div>
            </section>
            <section>
                <div className="container">
                    <div className="row">
                        <div className="col-md-4">
                            <form action="">
                                <div className="mb-2">
                                    <input type="email" className="form-control" placeholder="Email"/>
                                </div>
                                <div className="mb-2">
                                    <input type="password" className="form-control" placeholder="Password"/>
                                </div>

                                <div className="mb-2">
                                    <input type="submit" className="btn btn-success btn-sm" value='Login'/>
                                </div>
                            </form>
                            <small>
                                Don't have an Account ?
                                <NavLink to={'/user/register'} className="text-success"> Register</NavLink>
                            </small>
                        </div>
                    </div>
                </div>
            </section>
        </React.Fragment>
    )
}

export default LoginUser