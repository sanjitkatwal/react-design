import React, {useState} from 'react'
import './App.css'
import Navbar from "./modules/layout/components/Navbar";
import {BrowserRouter, Route, Switch} from "react-router-dom";
import Home from './modules/layout/components/Home'
import Employees from './modules/employees/components/EmployeeList'
import CountryList from "./modules/countries/components/CountryList";
import About from "./modules/layout/components/About";
import LoginUser from "./modules/users/components/LoginUser";
import RegisterUser from "./modules/users/components/RegisterUser";
import EmployeeDetail from "./modules/employees/components/EmployeeDetail";


let App = () =>{

    return (

        <React.Fragment>
            <BrowserRouter>
                <Navbar/>
                <Switch>
                    <Route exact={true} path={'/'} component={Home}/>
                    <Route exact={true} path={'/employees/list'} component={Employees}/>
                    <Route exact={true} path={'/employee/:employeeID'} component={EmployeeDetail}/>
                    <Route exact={true} path={'/countries/list'} component={CountryList}/>
                    <Route exact={true} path={'/about'} component={About}/>
                    <Route exact={true} path={'/about'} component={About}/>
                    <Route exact={true} path={'/user/login'} component={LoginUser}/>
                    <Route exact={true} path={'/user/register'} component={RegisterUser}/>
                </Switch>
            </BrowserRouter>

        </React.Fragment>


    );
}

export default App;
